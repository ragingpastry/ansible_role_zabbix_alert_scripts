import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']
).get_hosts('zabbix-web')


def test_service(host):
    f = host.socket("tcp://0.0.0.0:8080")
    assert f.is_listening
